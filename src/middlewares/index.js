import promise from 'redux-promise-middleware';
import {applyMiddleware} from 'redux';

export default applyMiddleware(
  promise()
);
