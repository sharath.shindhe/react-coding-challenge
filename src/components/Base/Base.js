import React, { Component } from 'react';
import Calendar from '../Calendar/Calendar';

class Base extends Component {
  render () {
    return (
        <div className="app">
            <Calendar/>
        </div>
    );
  }
}

export default Base;
