import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveReminders, switchDate } from '../../../actions/creator';
import Month from '../Month/Month';
import { getMonthYear, getCurrentReminders } from '../../../utils/calendar';
import config from '../../../config';

class Grid extends Component {
	constructor () {
		super();
		this.saveReminders = this.saveReminders.bind(this);
		this.previousMonth = this.previousMonth.bind(this);
		this.nextMonth = this.nextMonth.bind(this);
	}
	saveReminders () {
		this.props.saveReminders();
	}
	previousMonth () {
		this.props.switchDate(-1);
	}
	nextMonth () {
		this.props.switchDate(1);
	}
	render () {
		const {date} = this.props;
		const month = getMonthYear(date.date);
		const reminders = getCurrentReminders(this.props.reminders, date.key);
		return (
			<div className="grid">
				<div className="calendar">
					<div className="box-head">
						<div className="new-event"
							onClick={this.saveReminders}>
							<span role="img" aria-label="save">💾</span>
						</div>
						<div className="new-event" onClick={this.previousMonth}>←</div>
						<div style={config.customStyles.calendarMonth}>
							{month}
						</div>
						<div style={config.customStyles.nextMonth} className="new-event" onClick={this.nextMonth}>→</div>
					</div>
					<Month date={date} reminders={reminders} />
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		date: state.date,
		reminders: state.reminders
	};
};

const mapDispatchToProps = dispatch => {
	return {
		saveReminders: () => dispatch(saveReminders()),
		switchDate: offset => dispatch(switchDate(offset))
	};
};

const GridClass = connect(mapStateToProps, mapDispatchToProps)(Grid);

export default GridClass;
