import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from '../../Modal/Modal';
import Form from '../Form/Form';
import './Overlay.css';
import config from '../../../config';
import { reminderFilter, getCurrentDate, getNewDate, getOverlayTitle } from '../../../utils/calendar';

class Overlay extends Component {
  constructor() {
    super();
    this.state = {...config.initialState.overlay};
    this.filterReminders = this.filterReminders.bind(this);
    this.getReminder = this.getReminder.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleDate = this.handleDate.bind(this);
  }
  filterReminders() {
    const {reminders, date, day} = this.props;
    return reminderFilter(reminders, date, day);
  }
  getReminder(id) {
    const reminders = this.filterReminders();
    const reminder = reminders.find(r => this.findReminder(r, id));
    this.setState({reminder: reminder ? reminder : {...config.initialState.reminder}});
  }
  findReminder(reminder, id) {
    return id ? reminder.id.toString() === id.toString() : false;
  }
  componentDidMount() {
    if (this.props.isUpdating) {
      this.getReminder(this.props.reminder);
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps.isUpdating !== this.props.isUpdating || prevProps.reminder !== this.props.reminder) {
      this.getReminder(this.props.reminder);
    }
  }
  handleClose() {
    this.setState({
      reminder: {...config.initialState.reminder},
      isUpdating: false
    }, () => this.props.closeModal());
  };
  handleChange(color) {
    const {reminder} = this.state;
    this.setState({ reminder: { ...reminder, color: color.hex}});
  };
  handleDescriptionChange(e) {
    const {reminder} = this.state;
    this.setState({ reminder: {...reminder, description: e.target.value}});
  }
  handleDate(date){
    const {reminder} = this.state;
    this.setState({ reminder: {...reminder, date: date._d}});
  }
  
  render() {
    const {isUpdating, date, day} = this.props;
    const {reminder} = this.state;
    const currentDate = getCurrentDate(date, day);
    const newDate = getNewDate(reminder.date, currentDate);
    const description = getOverlayTitle(reminder.description);
    return (
      <Modal isOpen={this.props.isOpen} closeModal={this.handleClose} style={config.customStyles.modal.overlay}>
        <p>
          <b>{currentDate} - {newDate} {description}</b>
          <button onClick={this.handleClose}>✖</button>
        </p>
        <Form
          isUpdating={isUpdating}
          reminder={reminder}
          changeColor={this.handleChange}
          onDescriptionChange={this.handleDescriptionChange}
          day={this.props.day}
          onClose={this.handleClose}
          onChangeDate={this.handleDate}
          currentDate={currentDate}
          />
      </Modal>
    );
  }
}

const mapStateToProps = state => {
  return {
    date: state.date.date
  }
};

const mapDispatchToProps = () => {
  return {

  };
};

const OverlayClass = connect(mapStateToProps, mapDispatchToProps)(Overlay);

export default OverlayClass;
