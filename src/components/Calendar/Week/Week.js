import React, { Component } from 'react';
import { connect } from 'react-redux';
import Day from '../Day/Day';
import {getFirstWeekStart, getFirstWeekEnd, getMidWeekStart, getMidWeekEnd, getLastWeekStart, getLastWeekEnd, isStartWeek, isMidWeek, isEndWeek, reminderFilter } from '../../../utils/calendar';

class Week extends Component {
  constructor () {
    super();
    this.renderDayCell = this.renderDayCell.bind(this);
    this.renderDay = this.renderDay.bind(this);
  }

  renderDay (key, day, disabled) {
    const {date} = this.props;
    const reminders = reminderFilter(this.props.reminders, date, day);
    return (
      <Day
        key={key}
        day={day}
        disabled={disabled}
        openModal={this.props.openModal}
        reminders={reminders}
        afterOpenModal={this.props.afterOpenModal}
        closeModal={this.props.closeModal}
        />
    );
  }

  renderDayCell (day) {
    return this.renderDay(day.key, day.day, day.disabled);
  }

  render () {
    const {day, date} = this.props;
    const startWeek = isStartWeek(day);
    const midWeek = isMidWeek(day, date);
    const endWeek = isEndWeek(day, date);
    return (
        <div className="row" key={this.props.key}>
          {startWeek && getFirstWeekStart(date).map(this.renderDayCell)}
          {startWeek && getFirstWeekEnd(date).map(this.renderDayCell)}
          {midWeek && getMidWeekStart(day, date).map(this.renderDayCell)}
          {midWeek && getMidWeekEnd(day, date).map(this.renderDayCell)}
          {endWeek && getLastWeekStart(day, date).map(this.renderDayCell)}
          {endWeek && getLastWeekEnd(date, day).map(this.renderDayCell)}
        </div>
      );
  }
}

const mapStateToProps = state => {
  return {
    date: state.date.date
  };
};

const mapDispatchToProps = () => {
  return {

  };
};

const WeekClass = connect(mapStateToProps, mapDispatchToProps)(Week);

export default WeekClass;
