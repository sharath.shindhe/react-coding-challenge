import React, { Component } from 'react';
import {DAYS_OF_THE_WEEK} from '../../../constants/values';
import Label from './Label/Label';

class Header extends Component {
  renderDay (day, i) {
    return <Label key={i} number={i} day={day}/>;
  }
  render () {
    return (
        <div className="row">
            {DAYS_OF_THE_WEEK.map(this.renderDay)}
        </div>
      );
  }
}

export default Header;
