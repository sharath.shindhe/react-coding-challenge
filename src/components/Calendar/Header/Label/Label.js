import React, { Component } from 'react';

class Label extends Component {
  render () {
    const {number, day} = this.props;
    return (
        <div className="dt" key={number}>{day}</div>
    );
  }
}

export default Label;
