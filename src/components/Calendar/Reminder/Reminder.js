import React, { Component } from 'react';
import config from '../../../config';
import { hexToRgba, getContrast, getDate } from '../../../utils/calendar';

class Reminder extends Component {
  render () {
    const {id, date, description, color} = this.props.reminder;
    const rgba = hexToRgba(color);
    const theme = getContrast(rgba);
    const content = getDate(date, description);
    return (
      <p
        id={id}
        key={id}
        style={{
          ...config.customStyles.reminder,
          backgroundColor: theme.backgroundColor,
          color: theme.color
        }}
        onClick={this.props.onClickModal}
        >
        {content}
      </p>
    );
  }
}

export default Reminder;
