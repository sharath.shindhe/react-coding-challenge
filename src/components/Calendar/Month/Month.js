import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../Header/Header';
import Week from '../Week/Week';
import Overlay from '../Overlay/Overlay';
import { getCalendarWeeks } from '../../../utils/calendar';
import config from '../../../config/index';

class Month extends Component {
  constructor () {
    super();
    this.state = {
      ...config.initialState.month
    };
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.renderWeek = this.renderWeek.bind(this);
  }
  openModal (day, isUpdating, id) {
    this.setState({day, isOpen: true, isUpdating, id});
  }
  afterOpenModal () {
    this.subtitle.style.color = config.afterOpenModal;
  }
  closeModal () {
    this.setState({day: null, isOpen: false, isUpdating: false, id: null});
  }
  renderWeek (day, i) {
    return (
      <Week
        day={day}
        key={i}
        openModal={this.openModal}
        afterOpenModal={this.afterOpenModal}
        closeModal={this.closeModal}
        reminders={this.props.reminders}
        />
    );
  }
  render () {
    const weeks = getCalendarWeeks(this.props.date);
    const {day, isUpdating, id} = this.state;
    return (
        <div className="month-view">
          <Header/>
          {weeks.map(this.renderWeek)}
          <Overlay
            isOpen={this.state.isOpen}
            closeModal={this.closeModal}
            day={day}
            isUpdating={isUpdating}
            reminder={id}
            reminders={this.props.reminders}
            />
        </div>
      );
  }
}

const mapStateToProps = state => {
  return {
    date: state.date.date
  };
};

const mapDispatchToProps = () => {
  return {
    
  };
};

const MonthClass = connect(mapStateToProps, mapDispatchToProps)(Month);

export default MonthClass;
