import React, { Component } from 'react';
import {connect} from 'react-redux';
import Grid from './Grid/Grid';
import './Calendar.css';
import { loadReminders } from '../../actions/creator';
import { FULFILLED } from 'redux-promise-middleware';

class Calendar extends Component {
  componentDidMount() {
    if (this.props.reminders.status !== FULFILLED) {
      this.props.loadReminders();
    }
  }
  render () {
    return (
        <div className="calendar">
            <Grid/>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    reminders: state.reminders
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadReminders: () => dispatch(loadReminders())
  };
};

const CalendarClass = connect(mapStateToProps,mapDispatchToProps)(Calendar);

export default CalendarClass;
