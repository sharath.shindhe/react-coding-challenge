import React, { Component } from 'react';
import { connect } from 'react-redux';
import { sortByDate, isToday } from '../../../utils/calendar';
import Reminder from '../Reminder/Reminder';
import config from '../../../config';
import { DAY_DISABLED_CLASS, DAY_TODAY_CLASS, DAY_CLASS } from '../../../constants/calendar';

class Day extends Component {
  constructor () {
    super();
    this.renderReminder = this.renderReminder.bind(this);
    this.handleReminderModal = this.handleReminderModal.bind(this);
    this.handleReminderUpdateModal = this.handleReminderUpdateModal.bind(this);
  }
  renderReminder (reminder, i) {
    return (
      <Reminder key={i} reminder={reminder} onClickModal={this.handleReminderUpdateModal}/>
    );
  }
  handleReminderModal () {
    const {disabled, day} = this.props;
    if (!disabled) {
      this.props.openModal(day, false);
    }
  }
  handleReminderUpdateModal (e) {
    e.stopPropagation();
    const {id} = e.target;
    const {disabled, day} = this.props;
    if (!disabled) {
      this.props.openModal(day, true, id);
    }
  }
  render () {
    const {day, disabled, key, date, reminders} = this.props;
    const today = isToday(date, day);
    const className = disabled ? DAY_DISABLED_CLASS : today ? DAY_TODAY_CLASS : DAY_CLASS;
    return (
      <div key={key} className={className} onClick={this.handleReminderModal}>
        {!disabled && <p className="number" style={config.customStyles.dayHeader}>{day}</p>}
        {!disabled && reminders.sort(sortByDate).map(this.renderReminder)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    date: state.date.date
  };
};

const mapDispatchToProps = () => {
  return {

  };
};

const DayClass = connect(mapStateToProps, mapDispatchToProps)(Day);

export default DayClass;
