import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addReminder, updateReminder, deleteReminder } from '../../../actions/creator';
import Datetime from 'react-datetime';
import moment from 'moment';
import { GithubPicker } from 'react-color';
import config from '../../../config/index';
import '../Overlay/Overlay.css';
import './Datetime.css';
import { DATE_FORMAT, UPDATE_REMINDER, ADD_REMINDER, CLOSE_MODAL, DELETE_REMINDER } from '../../../constants/calendar';

class Form extends Component {
  constructor () {
    super();
    this.state = {
      displayColorPicker: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleDisposal = this.handleDisposal.bind(this);
  }
  handleClick () {
    this.setState({ displayColorPicker: !this.state.displayColorPicker });
  }
  handleClose () {
    this.setState({ displayColorPicker: false });
  }
  handleSubmit () {
    const {description, date} = this.props.reminder;
    const {isUpdating, currentDate} = this.props;
    const reminder = {
      ...this.props.reminder,
      date: date ? moment(date)._d : new Date(currentDate)
    };
    if (isUpdating) {
      this.props.updateReminder(reminder.id, reminder);
      this.props.onClose();
    } else {
      if (!description) {
        this.props.onClose();
      } else {
        this.props.addReminder(reminder);
        this.props.onClose();
      }
    }
  }
  handleDisposal () {
    const { isUpdating, reminder } = this.props;
    if (isUpdating) {
      this.props.deleteReminder(reminder.id, reminder.date);
      this.props.onClose();
    } else {
      this.props.onClose();
    }
  }

  render () {
    const { isUpdating, reminder } = this.props;
    const { description, date } = reminder;
    const {displayColorPicker} = this.state;
    const currentDate = date ? moment(date)._d : new Date(this.props.currentDate);
    const color = { ...config.customStyles.colorpicker.color, backgroundColor: reminder.color };
    const {maxLength} = config.reminder;
    const submitText = isUpdating ? UPDATE_REMINDER : ADD_REMINDER;
    const cancelText = isUpdating ? DELETE_REMINDER : CLOSE_MODAL;
    return (
        <div className="form">
          <form>
            <label>Description</label>
            <input type="text" placeholder="Enter Description" maxLength={maxLength} value={description} onChange={this.props.onDescriptionChange} />
            <label htmlFor="description">Date and Time</label>
            <Datetime value={currentDate} onChange={this.props.onChangeDate} dateFormat={DATE_FORMAT} />
            <label htmlFor="color">Color</label>
            <div style={config.customStyles.colorpicker.swatch} onClick={this.handleClick}>
              <div style={color} />
            </div>
            <div>
              {displayColorPicker ? (
                <div style={config.customStyles.colorpicker.popover}>
                  <div style={config.customStyles.colorpicker.cover} onClick={this.handleClose} />
                    <GithubPicker
                      color={reminder.color}
                      onChange={this.props.changeColor}
                      colors={config.colors}
                      />
                  </div>
              ) : null}
              </div>
              <br />
              <input type="button" value={submitText} onClick={this.handleSubmit} />
              <input type="button" className="cancel" value={cancelText} onClick={this.handleDisposal} />
            </form>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    date: state.date.date
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addReminder: data => dispatch(addReminder(data)),
    deleteReminder: (id, date) => dispatch(deleteReminder(id, date)),
    updateReminder: (id, data) => dispatch(updateReminder(id, data)),
  };
};

const FormClass = connect(mapStateToProps, mapDispatchToProps)(Form);

export default FormClass;
