import ReactModal from 'react-modal';
import config from '../../config/index';

import React, { Component } from 'react';

class Modal extends Component {
  render () {
    return (
        <ReactModal
          isOpen={this.props.isOpen}
          ariaHideApp={false}
          style={config.customStyles.modal}
          >
          {this.props.children}
        </ReactModal>
    );
  }
}

export default Modal;
