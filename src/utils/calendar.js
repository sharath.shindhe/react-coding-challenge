import moment from 'moment';
import {MONTH_YEAR_FORMAT, DATE_TIME_FORMAT, MOMENT_DATE_FORMAT, MOMENT_TIME_FORMAT} from '../constants/calendar';

export const getKey = date => {
  const currentDate = new Date(date);
  const info = {
    month: currentDate.getMonth() + 1,
    year: currentDate.getFullYear()
  };
  info.month = info.month < 10 ? '0' + info.month : info.month;
  return info.month + '-' + info.year;
}

export const sortById = (reminder1, reminder2) => {
  return parseInt(reminder1.id) - parseInt(reminder2.id);
};

export const getMaxId = reminders => {
  const data = reminders.sort(sortById);
  if (data && data.length) {
    return data[data.length - 1].id;
  }
  return 0;
};

const getDayofDate = (startDate, date) => {
  date.setDate(startDate);
  return date.getDay();
}

const getLastDayOfTheMonth = date => new Date(date.getFullYear(), date.getMonth() + 1, 0);

const getFirstDayOfTheMonth = date => new Date(date.getFullYear(), date.getMonth(), 1);

export const getMonthYear = date => moment(date).format(MONTH_YEAR_FORMAT);

export const getCurrentReminders = (reminders, key) => {
  const filteredReminders = reminders && reminders.data ? reminders.data[key] : []
  return filteredReminders && filteredReminders.length ? filteredReminders : [];
}

export function getFirstWeekStart(date) {
  const firstDay = getFirstDayOfTheMonth(date).getDay();
  const days = [];
  for (let i = 0; i < firstDay; i++) {
    days.push({
      key: i,
      disabled: true,
      day: null
    });
  }
  return days;
}

export function getFirstWeekEnd (date) {
  const startDate = getFirstDayOfTheMonth(date);
  const firstDay = startDate.getDay();
  const firstDate = startDate.getDate();
  let counter = 0;
  const days = [];
  for (let i = firstDay; i <= 6; i++) {
    days.push({
      key: i,
      day: firstDate + (counter++),
      disabled: false
    });
  }
  return days;
}

export function getMidWeekStart (day, date) {
  const currentDay = getDayofDate(day, date);
  const days = [];
  for (let i = (day - currentDay); i < day; i++) {
    days.push({
      key: i,
      day: i,
      disabled: false
    });
  }
  return days;
}

export function getMidWeekEnd (day, date) {
  const currentDay = getDayofDate(day, date);
  const days = [];
  let counter = 0;
  for (let i = currentDay; i <= 6; i++) {
    days.push({
      key: i,
      day: day + (counter++),
      disabled: false
    });
  }
  return days;
}

export function getLastWeekStart (day, date) {
  const currentDay = getDayofDate(day, date);
  const days = [];
  for (let i = (day - currentDay); i <= day; i++) {
    days.push({
      key: i,
      day: i,
      disabled: false
    });
  }
  return days;
}

export function getLastWeekEnd (date, day) {
  const lastDate = getLastDayOfTheMonth(date).getDate();
  const lastDay = getLastDayOfTheMonth(date).getDay();
  const days = [];
  if (day < lastDate) {
    for (let i = (day + 1); i <= lastDate; i++) {
      days.push({
        key: i,
        disabled: false,
        day: i
      });
    }
  }
  for (let i = lastDay; i < 6; i++) {
    days.push({
      key: i,
      disabled: true,
      day: null
    });
  }
  return days;
};


export function getCalendarWeeks(date) {
  const lastDate = getLastDayOfTheMonth(date).getDate();
  const lastDay = getLastDayOfTheMonth(date).getDay();
  const weeks = [];
  for (let i = 1; i < lastDate; i += 7) {
    weeks.push(i);
    if ((i + 7) >= (lastDate)) {
      if (getDayofDate(i, date) >= lastDay) {
        weeks.push(lastDate);
      }
    }
  }
  return weeks;
};

export const isStartWeek = day => day === 1;
export const isMidWeek = (day, date) => {
  const startDay = getDayofDate(day, date);
  const lastDate = getLastDayOfTheMonth(date).getDate();
  return day > 1 && ((day + Math.abs(startDay - 6)) < lastDate);
};

export const isEndWeek = (day, date) => {
  const lastDate = getLastDayOfTheMonth(date).getDate();
  const startDay = getDayofDate(day, date);
  const lastDay = getLastDayOfTheMonth(date).getDay();
  return day > 1 && ((day === lastDate) || (((day + 7) > lastDate) && (startDay < lastDay)));
};

export const sortByDate = (a,b) => new Date(a.date) - new Date(b.date);

export const reminderFilter = (reminders, date, day) => {
  const currentDate = new Date(date);
  currentDate.setDate(day);
  const filteredReminders = reminders.filter(reminder => {
      const reminderDate = new Date(reminder.date);
      const month = currentDate.getMonth() === reminderDate.getMonth();
      const day = currentDate.getDate() === reminderDate.getDate();
      const year = currentDate.getFullYear() === reminderDate.getFullYear();
      return (day && month && year)
  });
  return filteredReminders;
};

export function hexToRgba (hex) {
  let c;
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
          c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = '0x' + c.join('');
      return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',1)';
  }
  return hex;
};

export function getContrast (value) {
  const rgb = value.substring(5, value.length - 1).split(',');
  const o = Math.round(((parseInt(rgb[0]) * 299) + (parseInt(rgb[1]) * 587) + (parseInt(rgb[2]) * 114)) / 1000);
  const color = (o > 125) ? 'black' : 'white';
  const backgroundColor = 'rgb(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ')';
  return {color, backgroundColor};
};

export function getDate(date, description) {
  return moment(date).format(DATE_TIME_FORMAT) + ' | ' + description;
};

export function isToday(date, day) {
  const propDate = new Date(date);
  const currentDate = new Date();
  return currentDate.getDate() === day && currentDate.getMonth() === propDate.getMonth() && currentDate.getFullYear() === propDate.getFullYear();
};

export function getCurrentDate(date, day) {
  const currentDate = new Date(date);
  currentDate.setDate(day);
  return moment(currentDate).format(MOMENT_DATE_FORMAT);
};

export function getNewDate(date, currentDate) {
  const newDate = date ? new Date(date) : new Date(currentDate);
  return moment(newDate).format(MOMENT_TIME_FORMAT);
};

export function getOverlayTitle(description) {
  return description ? (' | ' + description) : '';
};

