import {LOAD_REMINDERS, SWITCH_DATE, ADD_REMINDER, SAVE_REMINDERS, DELETE_REMINDER, UPDATE_REMINDER} from '../actions/types';

export const switchDate = offset => {
    return {
        type: SWITCH_DATE,
        offset
    }
};
export const loadReminders = () => {
  return {
      type: LOAD_REMINDERS
  };
};

export const addReminder = data => {
  return {
    type: ADD_REMINDER,
    data
  };
};

export const updateReminder = (id, data) => {
    return {
      type: UPDATE_REMINDER,
      id, 
      data
    };
};

export const deleteReminder = (id, date) => {
    return {
        type: DELETE_REMINDER,
        id,
        date
    };
};

export const saveReminders = () => {
  return {
    type: SAVE_REMINDERS
  };
};

