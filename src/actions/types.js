
export const SWITCH_DATE = 'SWITCH_DATE';
export const LOAD_REMINDERS = 'LOAD_REMINDERS';
export const ADD_REMINDER = 'ADD_REMINDER';
export const DELETE_REMINDER = 'DELETE_REMINDER';
export const UPDATE_REMINDER = 'UPDATE_REMINDER';
export const SAVE_REMINDERS = 'SAVE_REMINDERS';
export const UDPATE_REMINDER = 'UDPATE_REMINDER';
