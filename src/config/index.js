export default {
  BASE_PATH: 'http://localhost:3000',
  timeout: 3000,
  reminder: {
    maxLength: 25
  },
  colorpickerDefault: {
    r: 0,
    g: 0,
    b: 0
  },
  afterOpenModal: '#f00',
  colors: ['#B80000', '#DB3E00', '#BED3F3', '#D4C4FB', '#006B76', '#1273DE', '#004DCF', '#5300EB', '#EB9694', '#FAD0C3', '#FEF3BD', '#C1E1C5', '#BEDADC', '#C4DEF6'],
  customStyles: {
    calendarMonth: { textAlign: 'center', color: 'ghostwhite', paddingTop: '10px' },
    nextMonth: { float: 'right', marginRight: '50px', marginTop: '-24px', position: 'relative' },
    reminder: { fontSize: 'smaller', paddingLeft: '1.2em', marginBlockStart: '0.2em', marginBlockEnd: '0.2em' },
    dayHeader: {backgroundColor: '#1e1e25', position: 'sticky', paddingTop: '16px', marginTop: '0px', marginBlockStart: '0em', marginBlockEnd: '0em'},
    modal: {
      content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        overflow: 'none'
      },
      list: {
        height: '100px'
      },
      overlay: {overflow: 'none'}
    },
    colorpicker: {
      color: {
        width: '36px',
        height: '14px',
        borderRadius: '2px',
      },
      swatch: {
        marginLeft: '10px',
        marginTop: '10px',
        background: '#fff',
        borderRadius: '1px',
        border: '1px solid grey',
        boxShadow: '0 5px 5px 1px dimgrey',
        display: 'inline-block',
        cursor: 'pointer'
      },
      popover: {
        position: 'absolute',
        zIndex: '2'
      },
      cover: {
        position: 'fixed',
        top: '0px',
        right: '0px',
        bottom: '0px',
        left: '0px'
      }
    }
  },
  initialState: {
    month: {
      day: null,
      isOpen: false,
      id: null,
      isUpdating: false
    },
    reminder: {
      id: null,
      color: '#FFFFFF',
      description: '',
      date: null
    },
    overlay: {
      day: null,
      isOpen: false,
      reminder: {
        id: null,
        color: '#FFFFFF',
        description: '',
        date: null
      },
      reminders: [],
      isUpdating: false
    }
  }
};
