
export const MONTH_YEAR_FORMAT = 'MMMM - YYYY';

export const DATE_TIME_FORMAT = 'HH:mm';

export const DATE_FORMAT = 'DD-MM-YYYY';

export const MOMENT_DATE_FORMAT = 'll';
export const MOMENT_TIME_FORMAT = 'hh:mm A';

export const DAY_CLASS = 'day';
export const DAY_DISABLED_CLASS = 'day ntm';
export const DAY_TODAY_CLASS = 'day today';

export const ADD_REMINDER = 'Add';
export const UPDATE_REMINDER = 'Update';
export const DELETE_REMINDER = 'Delete';
export const CLOSE_MODAL = 'Cancel';
