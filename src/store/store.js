import {createStore} from 'redux';
import rootReducer from '../reducers/index';
import middlewares from '../middlewares/index';

export default function configureStore() {
  const store = createStore(rootReducer, {}, middlewares);
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = rootReducer;
      store.replaceReducer(nextReducer);
    });
  }
  return store;
};
