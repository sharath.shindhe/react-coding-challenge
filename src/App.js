import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Base from './components/Base/Base';
import store from './store/store';

import './App.css';

class App extends Component {
  render() {
    return (
      <Provider store={store()}>
        <Router>
          <Route path="/" component={Base} />
        </Router>
      </Provider>

    );
  }
}

export default App;
