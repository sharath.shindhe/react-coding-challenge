import { LOAD_REMINDERS, ADD_REMINDER, SAVE_REMINDERS, DELETE_REMINDER, UPDATE_REMINDER } from '../actions/types';
import { FULFILLED } from 'redux-promise-middleware';
import moment from 'moment';
import { getKey, getMaxId } from '../utils/calendar';

const initialState = {
  data: {},
  status: null
};

export default function reminders(state = initialState, action) {
  switch (action.type) {

    case LOAD_REMINDERS: {
      const keys = Object.keys(localStorage);
      const reminders = {};
      keys.map(month => {
        const data = JSON.parse(localStorage[month]);
        reminders[month] = data && data.length ? [...data] : [];
        return null;
      });
      return {...state, data: reminders, status: FULFILLED};
    }

    case SAVE_REMINDERS: {
      const {data} = state;
      const keys = Object.keys(data);
      keys.map(month => {
        const reminders = JSON.stringify(data[month]);
        localStorage.setItem(month, reminders);
        return null;
      });
      return state;
    }

    case DELETE_REMINDER: {
      const {id, date} = action;
      const key = getKey(date);
      const reminders = {...state.data};
      const month = [...reminders[key].filter(redminder => redminder.id.toString() !== id.toString())]
      reminders[key] = month;
      return {
        status: FULFILLED,
        data: reminders
      };
    }

    case ADD_REMINDER: {
      const {data} = action;
      if (!data.description || !moment(data.date).isValid) {
        return state;
      }
      const key = getKey(data.date);
      const reminders = {...state.data};
      const currentReminders = reminders ? reminders[key] : [];
      const currentMonth = currentReminders && currentReminders.length ? currentReminders : [];
      const id = parseInt(getMaxId(currentMonth)) + 1;
      const reminder = {
        ...data,
        id
      };
      const month = [...currentMonth, reminder]
      reminders[key] = month;
      return {
        status: FULFILLED,
        data: reminders
      };
    }

    case UPDATE_REMINDER: {
      const {id, data} = action;
      const key = getKey(data.date);
      const reminders = {...state.data};
      const month = [...reminders[key].filter(redminder => redminder.id.toString() !== id.toString()), data]
      reminders[key] = month;
      return {
        status: FULFILLED,
        data: reminders
      };
    }

    default: return state;
  }
};
