import { SWITCH_DATE } from '../actions/types';
import moment from 'moment';
import { getKey } from '../utils/calendar';

const currentDate = new Date();

const initialState = {
  date: currentDate,
  key: getKey(currentDate),
  offset: 0
};

export default function date(state = initialState, action) {
  switch (action.type) {
    case SWITCH_DATE: {
      const date = moment(new Date()).add(state.offset + action.offset, 'months')._d;
      return {
        ...state,
        date, 
        key: getKey(date),
        offset: state.offset + action.offset
      };
    }
    default:
      return state;
  }
}
