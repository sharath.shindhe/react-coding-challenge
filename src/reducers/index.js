import {combineReducers} from 'redux';
import date from './date';
import reminders from './reminders';

const rootReducer = combineReducers({
  reminders,
  date
});

export default rootReducer;
